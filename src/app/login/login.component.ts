import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model = new User('','','','');
  submitted = false;
  currentState = "loading ...";
  found: boolean;

  onSubmit() {
    this.submitted = true;
    console.log(sessionStorage.length);
    for (let i = 0; i < sessionStorage.length; i++){
      if(sessionStorage.key(i) == this.model.username) {
        this.found = true;
        if(this.checkPassword(sessionStorage.getItem(this.model.username), this.model.password)) {
          this.currentState = "Login successful! You will now get redirected!";
          sessionStorage.setItem("loggedInUser", this.model.username);
          setTimeout(function() {
            location.href = "/view";
          }, 5000);
        } else {
          this.currentState = "Login failed! Try remembering your password!";
        }
      }
    }
    if(!this.found) {
      this.currentState = "User was not found! Try registering!";
      setTimeout(function() {
        location.href = "/login";
      }, 5000);
    }
  }

  checkPassword(realPassword: string, inputPassword: string): boolean {
    let item = JSON.parse(realPassword);
    if(item.password === inputPassword) {
      return true;
    }
    return false;
  }

  getDiagnostic() {
    return JSON.stringify(this.model);
  }

  constructor() { }

  ngOnInit() {
  }

  redirecttomain() {
    location.href = "/";
  }
}
