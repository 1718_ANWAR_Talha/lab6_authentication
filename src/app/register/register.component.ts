import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model = new User('Max','Mustermann','maxmustermann','');
  submitted = false;
  samePW: boolean;
  secpw = this.model.password;
  newUser: User;
  currentState = "loading ...";
    
  onSubmit() {
    this.submitted = true;    
    this.newUser = new User(this.model.firstname, this.model.lastname, this.model.username, this.model.password);
    let myitem = sessionStorage.getItem(this.newUser.username);
    if(this.newUser.username == "loggedInUser") {
      this.currentState = "Not allowed username! Choose another one!";
    } else if(myitem != null) {
      this.currentState =  "User already exists!";
    } else {
      sessionStorage.setItem(this.newUser.username, JSON.stringify(this.newUser));
      this.currentState = "User was added! Try logging in!";
      setTimeout(function() {
        location.href = "/login";
      }, 5000);
    }
  }

  get diagnostic() {
    return JSON.stringify(this.model);
  }

  constructor() { }

  ngOnInit() {
  }

  checkSamePW(secPW: string) {
    this.secpw = secPW;
    if(this.model.password === secPW) {
      this.samePW = true;
    } else {
      this.samePW = false;
    }
  }

  redirecttomain() {
    location.href = "/";
  }
}
