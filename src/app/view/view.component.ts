import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  currentState = "loading ...";
  allowedToView = false;
  currentUser: User;

  constructor() {
    let item = sessionStorage.getItem("loggedInUser");
    let useritem = sessionStorage.getItem(item);
    if(item == null || item == "") {
      this.currentState = "You are not logged in! Log in before trying to access!";
      setTimeout(function() {
        location.href = "/login";
      }, 5000);
    } else {
      var user = JSON.parse(useritem);
      this.currentUser = new User(user.firstname, user.lastname, user.username, user.password);
      this.allowedToView = true;
    }
  }

  ngOnInit() {
  }

  logout() {
    sessionStorage.removeItem("loggedInUser");
    this.currentState = "You have logged out and will be redirected to the main page!";
    this.allowedToView = false;
    setTimeout(function() {
      location.href = "/";
    }, 5000);
  }

  redirecttomain() {
    location.href = "/";
  }
}
